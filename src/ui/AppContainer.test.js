import React from 'react';
import {
  AppContainer,
  buildUrl,
  mapDispatchToProps,
  mapStateToProps
} from './AppContainer';
import { shallow } from 'enzyme';
import configureMockStore from 'redux-mock-store';
import { bindActionCreators } from 'redux';
import { fetchAndHandleAjax } from '../redux/ajax';
import { saveCarOfTheWeek } from '../redux/carOfTheWeek';
import { saveMakes } from '../redux/makes';
import { saveModels } from '../redux/models';

describe('<AppContainer />', () => {
  const createWrapper = () => {
    const props = {
      fetchAndHandleAjax: () => {},
      saveCarOfTheWeek: () => {},
      saveMakes: () => {},
      saveModels: () => {}
    };
    return shallow(<AppContainer {...props} />);
  };

  it('stopLoading() works', () => {
    const wrapper = createWrapper();
    expect(wrapper.state().loading).toBeTruthy();
    wrapper.instance().stopLoading();
    expect(wrapper.state().loading).toBeFalsy;
  });

  it('stopFetching() works', () => {
    const wrapper = createWrapper();
    expect(wrapper.state().fetching).toBeTruthy();
    wrapper.instance().stopFetching();
    expect(wrapper.state().fetching).toBeFalsy;
  });

  it('minLoadingTimesUp() works', () => {
    const wrapper = createWrapper();
    // fetch false and loading true, times up
    wrapper.setState({ fetching: false, loading: true });
    wrapper.instance().minLoadingTimesUp();
    expect(wrapper.state().fetching).toBeFalsy();
    expect(wrapper.state().loading).toBeFalsy();
    // fetch true and loading true, times up
    wrapper.setState({ fetching: true, loading: true });
    wrapper.instance().minLoadingTimesUp();
    expect(wrapper.state().fetching).toBeTruthy();
    expect(wrapper.state().loading).toBeTruthy();
  });

  it('stopLoadingWithCondition() works', () => {
    const wrapper = createWrapper();
    // fetch false and loading true, times up
    wrapper.setState({ passedMinLoadingTime: false, loading: true });
    wrapper.instance().stopLoadingWithCondition();
    expect(wrapper.state().loading).toBeTruthy();
    // fetch true and loading true, times up
    wrapper.setState({ passedMinLoadingTime: true, loading: true });
    wrapper.instance().stopLoadingWithCondition();
    expect(wrapper.state().loading).toBeFalsy();
  });
});

describe('helpers', () => {
  it('buildUrl() returns a valid url', () => {
    const url = '123';
    expect(buildUrl(url)).toEqual(`${process.env.PUBLIC_URL}/mock/${url}.json`);
  });
});

describe('redux connections', () => {
  it('mapStateToProps() returns a valid object', () => {
    expect(mapStateToProps()).toEqual({});
  });
  it('mapDispatchToProps() returns a valid object', () => {
    const { dispatch } = configureMockStore();
    const mockDispatch = dispatch =>
      bindActionCreators(
        {
          fetchAndHandleAjax: fetchAndHandleAjax,
          saveCarOfTheWeek: saveCarOfTheWeek,
          saveMakes: saveMakes,
          saveModels: saveModels
        },
        dispatch
      );
    expect(JSON.stringify(mapDispatchToProps(dispatch))).toBe(
      JSON.stringify(mockDispatch(dispatch))
    );
  });
});
