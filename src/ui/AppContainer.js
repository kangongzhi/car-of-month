import React, { Component } from 'react';
import { fetchAndHandleAjax } from '../redux/ajax';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { sendGetRequest } from '../helpers/apis';
import App from './App';
import { saveCarOfTheWeek } from '../redux/carOfTheWeek';
import { saveMakes } from '../redux/makes';
import { saveModels } from '../redux/models';
import { withRouter } from 'react-router-dom';
import Loader from './components/Loader/Loader';
// helper
/*************************************************************/
const buildUrl = url => `${process.env.PUBLIC_URL}/mock/${url}.json`;
const CARS_OF_THE_WEEK_URL = buildUrl('carOfTheWeek');
const MAKES_URL = buildUrl('makes');
const MODELS_URL = buildUrl('models');

const minLoadingTime = 600; //ms
const initState = {
  loading: true,
  fetching: true,
  passedMinLoadingTime: false
};

const loadingFalse = () => ({ loading: false });
const fetchingFalse = () => ({ fetching: false });
// component
/*************************************************************/
export class AppContainer extends Component {
  state = initState;

  stopLoading = () => {
    this.setState(loadingFalse);
  };

  stopFetching = () => {
    this.setState(fetchingFalse);
  };

  minLoadingTimesUp = () => {
    if (this.state.fetching === false) {
      this.stopLoading();
    }
  };

  startMinLoadingTimer = minLoadingTime => {
    setTimeout(this.minLoadingTimesUp, minLoadingTime);
  };

  stopLoadingWithCondition = () => {
    if (this.state.passedMinLoadingTime === true) {
      this.stopLoading();
    }
  };

  componentDidMount() {
    const {
      fetchAndHandleAjax,
      saveCarOfTheWeek,
      saveMakes,
      saveModels
    } = this.props;

    const fetchMakes = () => sendGetRequest(fetchAndHandleAjax, MAKES_URL);

    const fetchModels = () => sendGetRequest(fetchAndHandleAjax, MODELS_URL);

    const fetchCarOfTheWeek = () =>
      sendGetRequest(fetchAndHandleAjax, CARS_OF_THE_WEEK_URL);

    this.startMinLoadingTimer(minLoadingTime);

    Promise.all([
      fetchMakes(),
      fetchModels(),
      fetchCarOfTheWeek()
    ]).then(values => {
      saveMakes(values[0]);
      saveModels(values[1]);
      saveCarOfTheWeek(values[2]);

      this.stopFetching();

      this.stopLoadingWithCondition();
    });
  }
  render() {
    return this.state.loading ? <Loader /> : <App />;
  }
}
// connect to store
/*************************************************************/
const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchAndHandleAjax: fetchAndHandleAjax,
      saveCarOfTheWeek: saveCarOfTheWeek,
      saveMakes: saveMakes,
      saveModels: saveModels
    },
    dispatch
  );

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(AppContainer)
);
// export for unit testing
/*************************************************************/
export {
  minLoadingTime,
  mapStateToProps,
  mapDispatchToProps,
  buildUrl,
  CARS_OF_THE_WEEK_URL,
  MAKES_URL,
  MODELS_URL,
  initState,
  loadingFalse,
  fetchingFalse
};
