import React, { Suspense, lazy } from 'react';
import { Link, Route } from 'react-router-dom';
import styles from './App.module.scss';
import Nav from './components/Nav/Nav';
import logo from './qantas-money_2x.png';
import Loader from './components/Loader/Loader';
// code splitting
/*************************************************************/
const loadCarOfWeekContainer = () =>
  import('./pages/car-of-week/CarOfTheWeekContainer');
const loadSearchContainer = () => import('./pages/search/SearchContainer');
const loadModelDetailsContainer = () =>
  import('./pages/model-details/ModelDetailsContainer');

const CarOfWeekContainer = lazy(loadCarOfWeekContainer);
const SearchContainer = lazy(loadSearchContainer);
const ModelDetailsContainer = lazy(loadModelDetailsContainer);
// component
/*************************************************************/
const App = () => (
  <div className="App">
    <header className={`${styles.header} flex items-center flex-wrap`}>
      <Link to={'/'}>
        <img src={logo} alt="Logo" className={styles.logo} />
      </Link>
      <Nav />
    </header>
    <main>
      <Suspense fallback={<Loader />}>
        <Route exact path="/" component={CarOfWeekContainer} />
        <Route path="/search" component={SearchContainer} />
        <Route path="/make/model/:id" component={ModelDetailsContainer} />
      </Suspense>
    </main>
  </div>
);

export default App;
// export for unit testing
/*************************************************************/
export {
  loadCarOfWeekContainer,
  loadModelDetailsContainer,
  loadSearchContainer
};
