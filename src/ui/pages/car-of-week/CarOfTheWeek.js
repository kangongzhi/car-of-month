import React from 'react';
import { formattedMoney } from '../../../helpers/formatters';
import styles from './carOfTheWeek.module.scss';
import Hero from '../../components/Hero/Hero';

const CarOfTheWeek = ({ car: { imageUrl, name, review, price, makeName } }) => (
  <Hero imageUrl={imageUrl}>
    <div className={`flex justify-center ${styles['car-details']}`}>
      <p>{name}</p>
      <p>{makeName}</p>
      <p>{`$${formattedMoney(price)}`}</p>
    </div>
    <blockquote>
      <q className={styles.review}>{review}</q>
    </blockquote>
  </Hero>
);

export default CarOfTheWeek;
