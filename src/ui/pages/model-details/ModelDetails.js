import React from 'react';
import { formattedMoney } from '../../../helpers/formatters';
import Hero from '../../components/Hero/Hero';
import style from './ModelDetails.module.scss';

const ModelDetails = ({
  car: { imageUrl = '', name = '', price = '', make = '' }
}) => (
  <Hero imageUrl={imageUrl}>
    <div className={`${style.text} flex justify-around`}>
      <p>{name}</p>
      <p>{make}</p>
      <p>{`$${formattedMoney(price)}`}</p>
    </div>
  </Hero>
);

export default ModelDetails;
