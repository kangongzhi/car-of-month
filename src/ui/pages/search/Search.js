import React from 'react';
import Select from '../../components/Select/Select';
import styles from './Search.module.scss';
// helpers
/*************************************************************/
// todo if only for testing use a separate export
const Row = ({ children, className }) => (
  <div className={`clearfix ${className}`}>{children}</div>
);
const Col = ({ children, className }) => (
  <div className={`sm-col sm-col-6 ${styles.col} ${className}`}>{children}</div>
);
// component
/*************************************************************/
const Search = ({
  handleMakesChange,
  handleModelsChange,
  handleClick,
  makes = [],
  models = [],
  hasSelectedMakeId,
  hasSelectedModelId
}) => (
  <Row className={styles.search}>
    <Row>
      <Col>
        <Select
          options={makes}
          handleOnChange={handleMakesChange}
          placeholder={'Choose make'}
        />
      </Col>
      <Col>
        <Select
          options={models}
          handleOnChange={handleModelsChange}
          placeholder={'Choose model'}
          disabled={hasSelectedMakeId === false}
        />
      </Col>
    </Row>
    <Row>
      <Col className={'sm-col-12'}>
        <button
          onClick={handleClick}
          className={`${styles.button} right`}
          disabled={hasSelectedModelId === false}
        >
          Search
        </button>
      </Col>
    </Row>
  </Row>
);

export default Search;
// export for unit testing
/*************************************************************/
export { Row, Col };
