import React from 'react';
import styles from './Image.module.scss';
import placeholder from './car-placeholder.png';
// helper
/*************************************************************/
const handleError = e => {
  e.target.src = placeholder;
};
// component
/*************************************************************/
const Image = ({ src, className = '' }) => (
  <img
    src={src}
    className={`${styles.image} ${className}`}
    onError={handleError}
    alt={''}
  />
);

export default Image;
// export for unit testing
/*************************************************************/
export { handleError };
