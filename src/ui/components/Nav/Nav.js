import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './Nav.module.scss';

const Nav = ({ className = '' }) => (
  <ul className={`${styles['list-unstyled']} flex ${className}`}>
    <li>
      <NavLink to="/" exact>
        Home
      </NavLink>
    </li>
    <li>
      <NavLink to="/search">search</NavLink>
    </li>
  </ul>
);

export default Nav;
