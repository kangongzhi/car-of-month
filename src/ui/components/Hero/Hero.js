import React from 'react';
import Image from '../Image/Image';
import styles from './Hero.module.scss';

const Hero = ({ imageUrl, children }) => (
  <div className={styles.hero}>
    <Image src={imageUrl} className={styles.image} />
    <div className={`${styles['text-holder-container']} flex items-center justify-end`}>
      <div className={`${styles['text-holder']} flex items-center flex-column`}>{children}</div>
    </div>
  </div>
);

export default Hero;
