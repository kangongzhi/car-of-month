import React from 'react';
import styles from './Select.module.scss';

const Select = ({
  options = [],
  handleOnChange,
  placeholder = '',
  className = '',
  disabled
}) => (
  <select
    onChange={handleOnChange}
    className={`${styles.select} ${className}`}
    disabled={disabled}
  >
    <option value="">{placeholder}</option>
    {options.map(({ id, name }) => (
      <option value={id} key={id}>
        {name}
      </option>
    ))}
  </select>
);

export default Select;
