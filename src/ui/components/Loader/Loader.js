import React from 'react';
import styles from './Loader.module.scss';

const Loader = () => (
  <div className={`${styles.loader} flex justify-center items-center`}>
    Loading...
  </div>
);

export default Loader;
