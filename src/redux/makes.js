// actions
/*************************************************************/
const SAVE = '@makes/save';

export const saveMakes = makes => ({
  type: SAVE,
  makes
});
// reducer
/*************************************************************/
const initialState = [];

const makes = (state = initialState, action) => {
  switch (action.type) {
    case SAVE:
      return action.makes;
    default:
      return state;
  }
};

export default makes;
// export for unit testing
/*************************************************************/
export { SAVE, initialState };
